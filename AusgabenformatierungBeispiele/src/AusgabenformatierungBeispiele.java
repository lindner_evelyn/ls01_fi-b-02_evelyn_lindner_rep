import javax.sound.midi.Soundbank;

public class AusgabenformatierungBeispiele {

    public static long factorial(int number) {
        long result = 1;

        for (int factor = 2; factor <= number; factor++) {
            result *= factor;
        }

        return result;
    }


    public static void main(String[] args) {
        double[] Fahrenheit = {-20,-10,0,20,30};
        double[] Celsius = {-28.889,-23.333,-17.7778,-6.6667,-1.1111};
        System.out.printf("%-12s%s%10s\n", "Fahrenheite","|","Celsius");
        for (int i=0; i<23; i++){
            System.out.print("-");
        }
        for(int i=0; i<Fahrenheit.length;i++){
            System.out.printf("\n%+-12.0f%s%10.2f",Fahrenheit[i],"|",Celsius[i]);
        }
        System.out.println();
        boolean doSecond =false;
        for (int i=0; i<2;i++){
            System.out.println("   **  ");
            if(doSecond==false){
            for (int n=0; n<2;n++){
                System.out.println("*      *");
            }
            doSecond=true;
            }
        }
        int giveNumber = 5;
        String  multiplikationString = "";
        for(int i=0; i<giveNumber+1; i++){
            if(i==0){
                System.out.printf("%-5s%s%-19s%s%4s\n",String.valueOf(i)+"!","=","","=",String.valueOf(factorial(i)));
            }else{
            multiplikationString += i;
            System.out.printf("%-5s%s%-19s%s%4s\n",String.valueOf(i)+"!","=",multiplikationString,"=",String.valueOf(factorial(i)));
            multiplikationString += " * ";
        }}
    }
}

public class TemperaturTabelle {

    public static void main(String[] args) {
        double[] Fahrenheit = {-20,-10,0,20,30};
        double[] Celsius = {-28.889,-23.333,-17.7778,-6.6667,-1.1111};
        System.out.printf("%-12s%s%10s\n", "Fahrenheite","|","Celsius");
        for (int i=0; i<23; i++){
            System.out.print("-");
        }
        for(int i=0; i<Fahrenheit.length;i++){
            System.out.printf("\n%+-12.0f%s%10.2f",Fahrenheit[i],"|",Celsius[i]);
        }
    }
}

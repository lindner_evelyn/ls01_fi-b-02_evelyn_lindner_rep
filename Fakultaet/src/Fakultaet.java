public class Fakultaet {
    public static long factorial(int number) {
        long result = 1;

        for (int factor = 2; factor <= number; factor++) {
            result *= factor;
        }

        return result;
    }
    public static void main(String[] args) {
        int giveNumber = 5;
        String  multiplikationString = "";
        for(int i=0; i<giveNumber+1; i++){
            if(i==0){
                System.out.printf("%-5s%s%-19s%s%4s\n",String.valueOf(i)+"!","=","","=",String.valueOf(factorial(i)));
            }else{
                multiplikationString += i;
                System.out.printf("%-5s%s%-19s%s%4s\n",String.valueOf(i)+"!","=",multiplikationString,"=",String.valueOf(factorial(i)));
                multiplikationString += " * ";
            }}
    }
}

import java.util.Arrays;
import java.util.Scanner;

class Fahrkartenautomat {

    public static void initializeArray(String[][] inputArray){
        for (String[] strings : inputArray) {
            Arrays.fill(strings, null);
        }
    }

    public static double[] appendArray(double[] inputArray){
        double[] outputArray = new double[inputArray.length+1];
        for(int i=0; i<inputArray.length;i++){
            outputArray[i]=inputArray[i];
        }
        return outputArray;
    }

    public static void rueckgabeEingezahlt(double[] zurueckgezahltesGeld){
        for (int i=1; i<zurueckgezahltesGeld.length;i++){
            konsolenOutputArray(muenzeAusgebenArray(zurueckgezahltesGeld[i]));
        }
    }

    //Methode zum Einholen eines UserInputs als Integer ohne Konsolenerror bei falschen Eingaben
    public static int scanningNextInputForInt(){
        //Integration des Scanners und einholen des Userinputs
        Scanner scanner = new Scanner(System.in);
        String userInputString= scanner.next();
        //Schleife zum Anschauen jedes Characters in dem Input String vom User
        for(int i=0; i<userInputString.length();i++){
            //Falls der aktuelle Character kein Zahl ist, wird eine Fehlermeldung ausgegeben und ein neues Input gefordert
            if(!(Character.isDigit(userInputString.charAt(i)))){
                System.out.println("Leider war Ihre Eingabe ungültig!\nErneute Eingabe: ");
                userInputString= scanner.next();
                i=-1;
            }
        }
        //Bei einem gültigen Zahlenformat umändern des Strings in einen Double Typ
        return Integer.parseInt(userInputString);
    }


    //Methode zum Einholen eines UserInputs als Double ohne Konsolen-Error bei falschen Eingaben
    public static double scanningNextInputForDouble(){
        //Integration des Scanners und einholen des Userinputs
        Scanner scanner = new Scanner(System.in);
        String userInputString= scanner.next();
        int kommaCounter=0;
        //Schleife zum Anschauen jedes Characters in dem Input String vom User
        for(int i=0; i<userInputString.length();i++){
            //Falls es keine Digit oder Komma ist, wird der User nach einem neuen Input aufgefordert und die Schleife beginnt von neu
            if(!(Character.isDigit(userInputString.charAt(i))) && !(userInputString.charAt(i) == '.' || userInputString.charAt(i) == ',')){
                System.out.println("Leider war Ihre Eingabe ungültig!\nErneute Eingabe: ");
                userInputString= scanner.next();
                i=-1;
                kommaCounter=0;
            }
            //Falls es Kommas oder Punkte als Kommas gibt, werden die einheitlich in den Doublestandard mit Punkten ersetzt
            else if((userInputString.charAt(i)=='.'||userInputString.charAt(i)==',')&&kommaCounter==0){
                userInputString = userInputString.replace(',','.');
                kommaCounter++;
            }
            //Wenn es mehr als eine Kommastelle gibt in der Zahl kommt es zur Fehlermeldung und der User muss erneut eine Zahl eingeben
            else if((userInputString.charAt(i)=='.'||userInputString.charAt(i)==',')&&kommaCounter>0){
                System.out.println("Leider war Ihre Eingabe keine gültige Zahl\nIhr Auswahl: ");
                userInputString= scanner.next();
                i=-1;
                kommaCounter=0;
            }
        }
        //Bei einem gültigen Zahlenformat umändern des Strings in einen Double Typ
        return Double.parseDouble(userInputString);
    }
    //Methode zum warten einer bestimmten Anzahl an Millisekunden vor der nächsten Aktion
    public static void warte(int millisekunde){
        try {
            Thread.sleep(millisekunde);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //Erstellen eines Arrays der die Geldscheinformatierung als String enthält
    public static String[] geldscheinAusgabe(int betrag){
        //Anlegen des String Arrays
        String [] geldscheinAusgabeText = new String[5];
        for(int i=0; i<5;i++){
            if(i==0||i==4){
                //Erste und letzte Zeile
                geldscheinAusgabeText[i]="+++++++++++++++++";
            }
            else if(i==1||i==3){
                //Äußere-mittlere Zeilen
                geldscheinAusgabeText[i]="+\t\t\t\t+";
            }
            else{
                //Zentrale Zeile mit dem Wert
                //Falls der Geldscheinwert kleiner als 10 wird ein extra Leerzeichen hinzugefügt
                if (betrag<10){
                    geldscheinAusgabeText[i]="+\t  "+betrag+"  EURO\t+";
                }
                //Alle Geldscheinbeträge die größer gleich sind als 10 EURO
                else{
                    geldscheinAusgabeText[i]="+\t "+betrag+"  EURO\t+";
                }
            }
        }
        return geldscheinAusgabeText;
    }

    //Erstellen eines Arrays der die Münzenformatierung als String enthält
    public static String[] muenzeAusgebenArray(double betrag){
        String einheit;
        if (betrag<=0.99){
            einheit = "CENT";
            betrag=betrag*100;
        }
        else{
            einheit="EURO";
        }
        //Anlegen des String Arrays
        String[] muenzenAusgabeText = new String[6];
        for(int i=0; i<6;i++){
            if(i==0||i==5){
                //Erste und letzte Zeile
                muenzenAusgabeText[i]="\t* * *\t";
            }else if(i==1||i==4){
                //Äußere-mittlere Zeilen
                muenzenAusgabeText[i]=" *\t\t   * ";
            }else if(i==2){
                //Erste mittelere Zeile mit dem Wert
                muenzenAusgabeText[i]="*\t  "+(int)betrag+" \t*";
            }else{
                //Zweite mittlere Zeile mit der Einheit
                muenzenAusgabeText[i]="*\t "+einheit+"\t*";
            }
        }
        return muenzenAusgabeText;
    }

    //Overloading zwei Methoden zur Ausgabe von einem Array
    //Zwei Dimensionale Array Ausgabe erfolgt von einer kompletten Zeile zur nächsten
    public static void konsolenOutputArray(String[][] outputString){
        //Schleife durch alle letzteren (untergesorteten)Array Elemente
        for(int i=0; i<outputString[0].length;i++){
            //Schleife durch alle anfangs (übergeordneten) Array Elemente
            //Somit geht er erst die Spalten in der ersten Zeile durch, danach springt er in die nächste Zeile
            for (String[] strings : outputString) {
                //Falls das Arrayelement leer ist (==Null), wird der Index übersprungen
                if (strings[i] == null) {
                    break;
                }
                System.out.print(strings[i]);
            }
            System.out.println();
        }
    }
    public static void konsolenOutputArray(String[] outputString){
        for (String s : outputString) {
            System.out.println(s);
        }
    }

    //Leeren der Kasse
    public static void kasseLeeren(double[][] wechselgeld){
        double gesamtesGeld = 0;
        //Aufrechnen aller Wechselgeldbeträge in der Kasse auf einen gesamten Geldbetrag
        for (double[] doubles : wechselgeld) {
            gesamtesGeld += doubles[0] * doubles[1];
        }
        //Entnahme des Wechselgeldes und Ausgeben der Münzen
        rueckgeldAusgeben(0,gesamtesGeld,wechselgeld);
    }

    //Auffüllen der Wechselgeldkasse
    public static void kasseAuffuellen(double[][] wechselgeld){
        //Schleife die jeden Geldbetrag im Wechselgeldkasten nacheinander abruft
        for(int i=0; i<wechselgeld.length;i++){
            //Konsolenausgabe zur Frage nach der Anzahl der aufzustockenden Geldbeträge
            System.out.println("\n\nBitte Wechselgeld eingeben:\n\tDie Anzahl der Geldbeträge von: "+wechselgeld[i][0]);
            //Einholen der Anzahl vom Adminuser
            int anzahlDesEingeworfnenWechselgelds = scanningNextInputForInt();
            //Hinzufügen der Geldbeträge in dem Wechselgeldkassen Array
            wechselgeld[i][1] += anzahlDesEingeworfnenWechselgelds;
        }
    }

    //Administrastions Menü zur Auswahl zusätzlicher Funktionen des Fahrkartenautomats
    public static void administrationsMenu(double[][] wechselgeld){

        //Ausgabetext des Headers
        String eingangsText= "Administrationsmenu";
        System.out.println(eingangsText);
        for (int i = 0; i < eingangsText.length(); i++)
        {
            System.out.print("=");
            warte(250);
        }

        //Abfrage nach User Input welche Option er benutzen möchte
        System.out.println(
                        "\n\nWelche Option wird benötigt?" +
                        "\n\tKasse leeren (1)" +
                        "\n\tKasse auffüllen (2)" +
                        "\n\tSchließen (<2)"
        );
        int adminInput = scanningNextInputForInt();

        //Bei Auswahl 1 abrufen der Methode zum leeren der Kasse
        if(adminInput==1){
            kasseLeeren(wechselgeld);
        }
        //Bei Auswahl 2 abrufen der Methode zum auffüllen der Wechselgeldkasse
        else if(adminInput==2){
            kasseAuffuellen(wechselgeld);
        }
    }

    //Methode zur Auswahl des Fahrscheins und Berechnung des Endpreises mithilfe der Anzahl und Einzelpreises
    public static double fahrkartenKaufen(double[][] wechselgeld){

        //benötigte Variablen der Methode
        double zuZahlenderBetrag=0;
        int ticketWahl=0;
        int nichtBezahlenVorErstenInput=0;
        int anzahlDerFahrscheine;

        //String Array zum einfüllen der verfügbaren Tickets
        String[] verfuegbareTickets= {
                "\n\tEinzelfahrschein Berlin AB (1)",
                "\n\tEinzelfahrschein Berlin BC (2)",
                "\n\tEinzelfahrschein Berlin ABC (3)",
                "\n\tKurzstrecke (4)","\n\tTageskarte Berlin AB (5)",
                "\n\tTageskarte Berlin BC (6)",
                "\n\tTageskarte Berlin ABC (7)",
                "\n\tKleingruppen-Tageskarte Berlin AB (8)",
                "\n\tKleingruppen-Tageskarte Berlin BC (9)",
                "\n\tKleingruppen-Tageskarte Berlin ABC (10)",
                "\n\tBezahlen (11)\n"
        };

        //Erste Digit ist der Preis und zweite Digit für die Anzahl an zu kaufenden Tickets
        double[][] ticketPreise = {
                {2.90,0}, //Preis, Anzahl - Einzelfahrschein Berlin AB
                {3.30,0}, //Preis, Anzahl - Einzelfahrschein Berlin BC
                {3.60,0}, //Preis, Anzahl - Einzelfahrschein Berlin ABC
                {1.90,0}, //Preis, Anzahl - Kurzstrecke
                {8.60,0}, //Preis, Anzahl - Tageskarte Berlin AB
                {9.00,0}, //Preis, Anzahl - Tageskarte Berlin BC
                {9.60,0}, //Preis, Anzahl - Tageskarte Berlin ABC
                {23.50,0},//Preis, Anzahl - Kleingruppen-Tageskarte Berlin AB
                {24.30,0},//Preis, Anzahl - Kleingruppen-Tageskarte Berlin BC
                {24.90,0},//Preis, Anzahl - Kleingruppen-Tageskarte Berlin ABC
        };

        //Ausgabetext des Headers
        String eingangsText= "Fahrkartenbestellvorgang";
        System.out.println(eingangsText);
        for (int i = 0; i < eingangsText.length(); i++)
        {
            //Ausgabezeichen für die komplette Zeile
            //Warte Funktion zum warten zwischen der Zeichensetzung jedes Characters in Millisekunden
            System.out.print("=");
            warte(250);
        }
        System.out.println("\n");

        //Wiederholung der Schleife für die Ticketauswahl solange der Kunde nicht Bezahlen auswählt
        while(true){

            //Ausgabe der verfügbaren Ticktes als Text
            System.out.println("Wählen Sie Ihre wünsch Fahrkarte aus:");
            for (String verfuegbareTicket : verfuegbareTickets) {
                System.out.print(verfuegbareTicket);
            }

            //Einholen der Ticketauswahl vom User
            System.out.print("Ihre Wahl: ");
            ticketWahl = scanningNextInputForInt();

            //Testen auf Gültigkeit der Eingabe, nur ein bereich von 1-10 erlaubt
            if(ticketWahl>0 && ticketWahl<11){
                //Einholen der Anzahl an benötigten Tickets vom User
                System.out.print("Anzahl der Tickets: ");
                anzahlDerFahrscheine = scanningNextInputForInt();
                //Ausgabe einer Fehlermeldung bei mehr als 10 Tickets auf einmal und zurücksetzen der Variable für die Ticketauswahl
                if (anzahlDerFahrscheine>10){
                    System.out.println("\nACHTUNG: ");
                    warte(300);
                    System.out.println("Leider sind das zu viele Fahrscheine, bitte probieren Sie nochmal!\n");
                    warte(1500);
                    ticketWahl=0;
                }
                else{
                    //Erhöhung der nichtBezahlenVorErstenInput Variable, sodass sie nach der ersten Ticketauswahl immer >1 ist
                    nichtBezahlenVorErstenInput++;
                    //Festlegen der Anzahl an ausgewählten Tickets innerhalb des Ticket-Arrays
                    ticketPreise[ticketWahl-1][1]=anzahlDerFahrscheine;
                }
            }else if(ticketWahl==1337){
                administrationsMenu(wechselgeld);
            }
            //Abbrechen der Ticketauswahl, sodass es weiter zum Bezahlvorgang gehen kann
            //Nur so lange die Auswahl der Zahl 11 entspricht und vorher eine Ticketauswahl getroffen wurde
            else if(ticketWahl==11 && nichtBezahlenVorErstenInput!=0){
                break;
            }
            //Konsolenausgabe einer Fehlermeldung falls User bezahlen möchte vor der ersten Ticketauswahl
            else if(ticketWahl == 11){
                System.out.println("\nACHTUNG: ");
                warte(300);
                System.out.println("Der Bezahlvorgang ist erst nach einer Ticketauswahl möglich\n");
                warte(1500);
                ticketWahl=0;
            }
            //Konsolenausgabe einer Fehlermeldung falls der User eine ungültige Eingabe tätigt
            //Zurücksetzen der User Eingabe auf 0
            else{
                System.out.println("\nACHTUNG: ");
                warte(300);
                System.out.println("Dieser Fahrschein existiert leider nicht.\n");
                warte(1500);
                ticketWahl=0;
            }
        }
        //Zusammenrechnen der einzelnen ausgewählten Tickets mit der Anzahl
        for (double[] doubles : ticketPreise) {
            zuZahlenderBetrag += doubles[0] * doubles[1];
        }
        //Rückgabe des zu zahlenden Preises für alle Tickets
        return zuZahlenderBetrag;
    }

    //Methode zum Bezahlen der Fahrkarten
    public static double[] fahrkartenBezahlen(double zuZahlenderBetrag){
        double[] eingezahlterGesamtbetrag= new double[1];
        eingezahlterGesamtbetrag[0]=0;
        //Wiederholung der Schleife so lange der User nicht alles abgezahlt hat
        while(eingezahlterGesamtbetrag[0] < zuZahlenderBetrag) {
            //Konsolenausgabe zur Aufforderung nach weiterem Geld
            System.out.printf("%s%.2f\n", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag[0]));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            double eingeworfeneMuenze = scanningNextInputForDouble();
            //Konsolenausgabe einer Fehlermeldung falls der User versucht andere Währung oder mehr als 2 EURO einzuwerfen
            if (eingeworfeneMuenze > 2) {
                System.out.println("ACHTUNG: Geldscheine oder andere Währungen werden nicht akzeptiert!");
            }
            else{
                //Hinzufügen der Münze zum Array
                eingezahlterGesamtbetrag=appendArray(eingezahlterGesamtbetrag);
                eingezahlterGesamtbetrag[eingezahlterGesamtbetrag.length-1]=eingeworfeneMuenze;
                //Aufrechnen des neuen Geldbetrags auf den totalen eingeworfenen Geldbetrag
                eingezahlterGesamtbetrag[0] += eingeworfeneMuenze;
            }
        }
        return eingezahlterGesamtbetrag;
    }

    /*
    //Methode zum Bezahlen der Fahrkarten
    public static double fahrkartenBezahlen(double zuZahlenderBetrag){
        double eingezahlterGesamtbetrag=0;
        //Wiederholung der Schleife so lange der User nicht alles abgezahlt hat
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            //Konsolenausgabe zur Aufforderung nach weiterem Geld
            System.out.printf("%s%.2f\n", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            double eingeworfeneMuenze = scanningNextInputForDouble();
            //Konsolenausgabe einer Fehlermeldung falls der User versucht andere Währung oder mehr als 2 EURO einzuwerfen
            if (eingeworfeneMuenze > 2) {
                System.out.println("ACHTUNG: Geldscheine oder andere Währungen werden nicht akzeptiert!");
            }
            else{
                //Aufrechnen des neuen Geldbetrags auf den totalen eingeworfenen Geldbetrag
                eingezahlterGesamtbetrag += eingeworfeneMuenze;
            }
        }
        return eingezahlterGesamtbetrag;
    }

     */

    //Abfrage ob genug Rückgeld vorhanden wäre, falls genug vorhanden ist wird TRUE zurückgegeben andernfalls FALSE
    public static boolean istGenugWechselgeldVorhanden(double[][] wechselgeld, double rueckgabeBetrag) {
        int wechselgeldInDerKasse;
        boolean enoughMoney=true;
        //Durchloopen der Wechselgeldkasse Arrays von hinten nach vorne
        for (int i = wechselgeld.length - 1; i >= 0; i--) {
            wechselgeldInDerKasse= (int)(wechselgeld[i][1]);
            //Falls der Rückgabebetrag größer als der aktuelle Wechselgeldbetrag ist, wird das so viel es geht damit abbzahlt
            while (rueckgabeBetrag >= wechselgeld[i][0]&&i>0) {
                //Nur so lange Wechselgeld vorhanden ist, kann welches rausgenommen werden
                if (wechselgeldInDerKasse > 0) {
                    rueckgabeBetrag -= wechselgeld[i][0];
                    rueckgabeBetrag = Math.round(rueckgabeBetrag*100);
                    rueckgabeBetrag = rueckgabeBetrag/100;
                    wechselgeldInDerKasse-=1;
                    enoughMoney=true;
                }
                //Wenn nicht genug Wechselgeld vorhanden war, schauen ob eine Ausgabe in einem Geldbetrag kleiner möglich ist
                else{
                    i--;
                    enoughMoney=false;
                }
            }
        }
        return enoughMoney;
    }

    //Konsolenausgabe der Fahrkarte
    public static void fahrkartenAusgeben(){
        // Fahrscheinausgabe
        // -----------------
        String ausgabeText = "Fahrschein wird ausgegeben";
        System.out.println("\n"+ausgabeText);
        //Konsolenausgabe von "=" der Länge des vorherigen Textes entsprechend
        for (int i = 0; i <ausgabeText.length(); i++)
        {
            System.out.print("=");
            warte(250);
        }
        System.out.println("\n");
    }

    //Konsolenausgabe des Rückgeldes und Berechnung des übrigen vorhandenem Kleingeldes
    public static double[][] rueckgeldAusgeben(double zuZahlenderBetrag, double eingezahlterGesamtbetrag, double[][] wechselgeld){
        //Berechnung des auszugebenden Rückgabebetrags
        double rueckgabeBetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        //Erstellen eines neuen Arrays für die Münzausgabe, sodass maximal 3 Münzen in einer Reihe ausgegeben werden
        String[][] rueckgabeBetragAusgabeText= new String[3][6];
        //Festlegen eines Zählers wie viele Münzen schon in einer Reihe sind
        int muenzenInEinerReihe = 0;
        //Testen ob ein Rückgabebetrag überhaupt vorhanden ist
        if(rueckgabeBetrag > 0.0)
        {
            //Konsolenausgabe des Textes zur Rückgeldausgabe
            System.out.printf("\n%s%.2f%s\n","Der Rückgabebetrag in Höhe von ",rueckgabeBetrag," Euro");
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            int wechselgeldArrayIndex = wechselgeld.length-1;

            //Ausführen der Schleife so lang der Rückgabebetrag in 50 EURO erfolgen kann und genug Wechselgeld vorhanden ist
            while(rueckgabeBetrag >= 49.999 && wechselgeld[wechselgeldArrayIndex][1]>0) // 50 EURO-Schein
            {
                konsolenOutputArray(geldscheinAusgabe(50));
                //Subtrahieren von 50 EURO vom aktuellen Rückgabebetrag
                //Jedes Mal eine Geldschein aus der Geldkasse rausnehmen
                wechselgeld[wechselgeldArrayIndex][1] -= 1;
                rueckgabeBetrag -= 50f;
            }
            wechselgeldArrayIndex--;

            //Ausführen der Schleife so lang der Rückgabebetrag in 20 EURO erfolgen kann und genug Wechselgeld vorhanden ist
            while(rueckgabeBetrag >= 19.999&&wechselgeld[wechselgeldArrayIndex][1]>0) // 20 EURO-Schein
            {
                konsolenOutputArray(geldscheinAusgabe(20));
                //Subtrahieren von 20 EURO vom aktuellen Rückgabebetrag
                //Jedes Mal eine Geldschein aus der Geldkasse rausnehmen
                wechselgeld[wechselgeldArrayIndex][1] -= 1;
                rueckgabeBetrag -= 20f;
            }
            wechselgeldArrayIndex--;

            //Ausführen der Schleife so lang der Rückgabebetrag in 10 EURO erfolgen kann und genug Wechselgeld vorhanden ist
            while(rueckgabeBetrag >= 9.999&&wechselgeld[wechselgeldArrayIndex][1]>0) // 10 EURO-Schein
            {
                konsolenOutputArray(geldscheinAusgabe(10));
                //Subtrahieren von 10 EURO vom aktuellen Rückgabebetrag
                //Jedes Mal eine Geldschein aus der Geldkasse rausnehmen
                wechselgeld[wechselgeldArrayIndex][1] -= 1;
                rueckgabeBetrag -= 10f;
            }
            wechselgeldArrayIndex--;

            //Ausführen der Schleife so lang der Rückgabebetrag in 5 EURO erfolgen kann und genug Wechselgeld vorhanden ist
            while(rueckgabeBetrag >= 4.999&&wechselgeld[wechselgeldArrayIndex][1]>0) // 5 EURO-Schein
            {
                konsolenOutputArray(geldscheinAusgabe(5));
                //Subtrahieren von 5 EURO vom aktuellen Rückgabebetrag
                //Jedes Mal eine Geldschein aus der Geldkasse rausnehmen
                wechselgeld[wechselgeldArrayIndex][1] -= 1;
                rueckgabeBetrag -= 5f;
            }
            wechselgeldArrayIndex--;

            //Ausführen der Schleife so lang der Rückgabebetrag in 2 EURO erfolgen kann und genug Wechselgeld vorhanden ist
            while(rueckgabeBetrag >= 1.999&&wechselgeld[wechselgeldArrayIndex][1]>0) // 2 EURO-Münzen
            {
                //Hinzufügen der nächsten Münzausgabe zu dem Konsolen Output Array
                if (muenzenInEinerReihe >= 3) {
                    //Ausgabe der drei vorherigen Münzen und initialisieren des Arrays zum speichern der Münzen
                    konsolenOutputArray(rueckgabeBetragAusgabeText);
                    initializeArray(rueckgabeBetragAusgabeText);
                    //Initialisieren der Array Index und Hinzufügen der neuen Münze danach erhöhen des Array Indexes
                    muenzenInEinerReihe = 0;
                }
                rueckgabeBetragAusgabeText[muenzenInEinerReihe]= muenzeAusgebenArray(2);
                muenzenInEinerReihe++;
                //Subtrahieren von 2 EURO vom aktuellen Rückgabebetrag
                rueckgabeBetrag -= 2f;
                //Jedes Mal eine Münze aus der Geldkasse rausnehmen
                wechselgeld[wechselgeldArrayIndex][1] -= 1;
            }
            wechselgeldArrayIndex--;

            //Analog zu der 2 EURO Schleife
            //Siehe Kommentar Zeile 427
            while(rueckgabeBetrag >= 0.999&&wechselgeld[wechselgeldArrayIndex][1]>0) // 1 EURO-Münzen
            {
                if (muenzenInEinerReihe >= 3) {
                    konsolenOutputArray(rueckgabeBetragAusgabeText);
                    initializeArray(rueckgabeBetragAusgabeText);
                    muenzenInEinerReihe = 0;
                }
                rueckgabeBetragAusgabeText[muenzenInEinerReihe] = muenzeAusgebenArray(1);
                muenzenInEinerReihe++;
                rueckgabeBetrag -= 1f;
                wechselgeld[wechselgeldArrayIndex][1] -= 1;
            }
            wechselgeldArrayIndex--;

            //Analog zu der 2 EURO Schleife
            //Siehe Kommentar Zeile 427
            while(rueckgabeBetrag >= 0.4999&&wechselgeld[wechselgeldArrayIndex][1]>0) // 50 CENT-Münzen
            {
                if (muenzenInEinerReihe >= 3) {
                    konsolenOutputArray(rueckgabeBetragAusgabeText);
                    initializeArray(rueckgabeBetragAusgabeText);
                    muenzenInEinerReihe = 0;
                }
                rueckgabeBetragAusgabeText[muenzenInEinerReihe]= muenzeAusgebenArray(0.5);
                muenzenInEinerReihe++;
                rueckgabeBetrag -= 0.5f;
                wechselgeld[wechselgeldArrayIndex][1] -= 1;
            }
            wechselgeldArrayIndex--;

            //Analog zu der 2 EURO Schleife
            //Siehe Kommentar Zeile 427
            while(rueckgabeBetrag >= 0.199&&wechselgeld[wechselgeldArrayIndex][1]>0) // 20 CENT-Münzen
            {
                if (muenzenInEinerReihe >= 3) {
                    konsolenOutputArray(rueckgabeBetragAusgabeText);
                    initializeArray(rueckgabeBetragAusgabeText);
                    muenzenInEinerReihe = 0;
                }
                rueckgabeBetragAusgabeText[muenzenInEinerReihe]= muenzeAusgebenArray(0.2);
                muenzenInEinerReihe++;
                rueckgabeBetrag -= 0.2f;
                wechselgeld[wechselgeldArrayIndex][1] -= 1;
            }
            wechselgeldArrayIndex--;

            //Analog zu der 2 EURO Schleife
            //Siehe Kommentar Zeile 427
            while(rueckgabeBetrag >= 0.099&&wechselgeld[wechselgeldArrayIndex][1]>0) // 10 CENT-Münzen
            {
                if (muenzenInEinerReihe >= 3) {
                    konsolenOutputArray(rueckgabeBetragAusgabeText);
                    initializeArray(rueckgabeBetragAusgabeText);
                    muenzenInEinerReihe = 0;
                }
                rueckgabeBetragAusgabeText[muenzenInEinerReihe]= muenzeAusgebenArray(0.1);
                muenzenInEinerReihe++;
                rueckgabeBetrag -= 0.1f;
                wechselgeld[wechselgeldArrayIndex][1] -= 1;
            }
            wechselgeldArrayIndex--;

            //Analog zu der 2 EURO Schleife
            //Siehe Kommentar Zeile 427
            while(rueckgabeBetrag >= 0.049&&wechselgeld[wechselgeldArrayIndex][1]>0)// 5 CENT-Münzen
            {
                if(muenzenInEinerReihe >= 3) {
                    konsolenOutputArray(rueckgabeBetragAusgabeText);
                    initializeArray(rueckgabeBetragAusgabeText);
                    muenzenInEinerReihe = 0;
                }
                rueckgabeBetragAusgabeText[muenzenInEinerReihe]= muenzeAusgebenArray(0.05);
                muenzenInEinerReihe++;
                rueckgabeBetrag -= 0.05;
                wechselgeld[wechselgeldArrayIndex][1] -= 1;
            }
            //Ausgabe der restlichen Münzen, falls nicht 3 Münzen in einer Reihe erreicht wurden
            if(muenzenInEinerReihe>0){
                konsolenOutputArray(rueckgabeBetragAusgabeText);
            }
        }
        //Rückgabe der restlichen Wechselgeldkasse
        return wechselgeld;
    }

    public static void fahrkartenAutomatenBetriebsablauf(double[][] wechselgeld, int ticketRohlinge, boolean errorHandler){
        //Benötigte Parameter in der Methode
        double zuZahlenderBetrag;
        double eingezahlterGesamtbetrag;
        //Der Ticketkauf wird so lange wiederholt bis ein Error gemeldet wird
        while(!errorHandler){
            //Zuordnung der Variablenwerte durch Verwendung der passenden Methoden
            zuZahlenderBetrag=fahrkartenKaufen(wechselgeld);
            double[] eingezahlterGesamtbetragMitEingabe = fahrkartenBezahlen(zuZahlenderBetrag);
            eingezahlterGesamtbetrag=eingezahlterGesamtbetragMitEingabe[0];

            //Ticketausgabe und Rückgeldberechnung nur möglich so lange genug Wechselgeld und genügen Ticketrohlinge vorhanden sind
            if (istGenugWechselgeldVorhanden(wechselgeld, eingezahlterGesamtbetrag - zuZahlenderBetrag) && ticketRohlinge>0){
                fahrkartenAusgeben();
                ticketRohlinge--;
                //Rückgeldausgabe erfolgt und abspeichern der neuen Wechselgeldkasse
                rueckgeldAusgeben(zuZahlenderBetrag,eingezahlterGesamtbetrag,wechselgeld);
                System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                        "vor Fahrtantritt entwerten zu lassen!\n"+
                        "Wir wünschen Ihnen eine gute Fahrt.");
            }
            //Fehlermeldung falls es keine Ticketrohlinge mehr gibt und Rückgabe des eingeworfenen Geldes
            else if(ticketRohlinge==0){
                rueckgeldAusgeben(0,eingezahlterGesamtbetrag,wechselgeld);
                System.out.println("Bitte benutzen Sie einen anderen Automaten bis zur nächsten Wartung!");
                errorHandler=true;
            }
            //Fehlermeldung falls nicht genug Wechselgeld vorhanden ist um den Rückbetrag passend auszugeben
            //wieder Rückausgabe des eingeworfenen Gelds
            else{
                rueckgabeEingezahlt(eingezahlterGesamtbetragMitEingabe);
                System.out.println("\nEs tut uns Leid, aber wir bitten Sie erneut passend zu zahlen!");
            }
            System.out.println("\n\n");
        }
    }

    public static void main(String[] args)
    {
        int ticketRohlinge = 100;
        //-> Anlegen der Wechselgeldkasse in einem Array, 1.Wert=Geldbetrag 2.Wert=Anzahl der Münzen/Geldscheine
        double[][] wechselgeld = {
                //  Betrag,    Anzahl
                {   0.05    ,  10   },
                {   0.1     ,  20   },
                {   0.2     ,  20   },
                {   0.5     ,  20   },
                {   1       ,  30   },
                {   2       ,  30   },
                {   5       ,  30   },
                {   10      ,  30   },
                {   20      ,  30   },
                {   50      ,  10   }
        };
        //->Error Handler um die Ticketkauf-Schleife bei bestimmten Fällen zu terminieren
        fahrkartenAutomatenBetriebsablauf(wechselgeld,ticketRohlinge, false);
    }
}
